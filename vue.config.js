module.exports = {
    devServer: {
        allowedHosts: 'all',
    },
    publicPath: "/",
    configureWebpack:{
        output: {
            libraryExport: 'default'
        }
    }
}