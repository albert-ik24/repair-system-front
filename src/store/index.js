import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    api_route:{
      baseUrl: 'https://back.amnez.site/api',
      login: '/login',
      register: '/register',
      repair: '/repair',
      comments: '/comments'
    },
    modalData: null,
    modalShow: false,
    modalType: null,
    workTypes: [],
    objectTypes: []
  },

  mutations: {
    setModalData(state, payload){
      state.modalData = payload
    },

    setModalShow(state, payload){
      state.modalShow = payload
    },

    setModalType(state, payload){
      state.modalType = payload
    },

    setWorkTypes(state, payload){
      state.workTypes = payload
    },

    setObjectTypes(state, payload){
      state.objectTypes = payload
    }
  },

  actions: {
  },

  getters: {
    getRoute: state => state.api_route,
    getModalData: state => state.modalData,
    getModalShow: state => state.modalShow,
    getModalType: state => state.modalType,
    getWorkTypes: state => state.workTypes,
    getObjectTypes: state => state.objectTypes
  }
})
