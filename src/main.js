import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueNoty from 'vuejs-noty'

Vue.use(VueNoty, {
  timeout: 1000,
  progressBar: true,
})
import 'vuejs-noty/dist/vuejs-noty.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')