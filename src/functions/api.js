export const api = async (url, method = 'GET', body = null, headers = {})=> {
    let res = await fetch(url, {
        method: method,
        body: body,
        headers: headers
    })

    return {
        data: res.json(),
        status: res.status
    }
}